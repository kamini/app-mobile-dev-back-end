const res = require("./res");
const db = require("./db");

function init(){
	console.log("Initializing");
	res();
	db();
	console.log("Initialised");
}


module.exports = init;