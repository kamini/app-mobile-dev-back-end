const fs = require("fs");
const config = require('../config/res');

module.exports = function(){
	if (!fs.existsSync(config.resDir)){
		fs.mkdirSync(config.resDir);
		console.log("Created resources directory");
	}
	if (!fs.existsSync(config.imagesDir)){
		fs.mkdirSync(config.imagesDir);
		console.log("Created images directory");
	}
	if (!fs.existsSync(config.soundDir)){
		fs.mkdirSync(config.soundDir);
		console.log("Created sound directory");
	}
	
}