const Image = require("../models/image");
const fs = require("fs");
const resConfig = require("../config/res");
const Order = require("../models/order");
const groupsConfig = require("../config/groups");

module.exports = async function(){
	try{
		let items = fs.readdirSync(resConfig.imagesDir)
		for (let i in items){
			let dbImage = await Image.findById(items[i]);
			if (dbImage == null){
				dbImage = new Image();
				dbImage._id = items[i];
				await dbImage.save();
			}
		}
	}catch(err){
		console.error(err);
	}


	let savedOrders = await Order.find({});
	for (let i in savedOrders){
		if (groupsConfig.wordSets.indexOf(savedOrders[i].wordSet) == -1){
			await savedOrders[i].remove();
		}
	}
	for (let i in groupsConfig.wordSets){
		if ((await Order.findOne({wordSet:groupsConfig.wordSets[i]}) == null)){
			let order = new Order();
			order.wordOrder = [];
			order.wordSet = groupsConfig.wordSets[i];
			await order.save();
		}
	}
}