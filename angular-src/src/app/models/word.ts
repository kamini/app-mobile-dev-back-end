import { Image } from "./image";

export class Word {
	private _id:string = "";
	word:string = "";
	soundGroups:Array<string> = [];
	images:Array<Image> = [new Image(), new Image(), new Image(), new Image()];
	similarWords: Array<string> = [];
	differentWords: Array<string> = [];
	wordSoundFile:string = null;
	soundGroupsSoundFile:string = null;
	description:string = "";
	sentenceGood:string = "";
	sentenceBad:string = "";
	matchingImages:Array<Image> = [];
	notMatchingImages:Array<Image> = [];

	constructor(data:any = null){
		if (data){
			this._id = data._id;
			this.word = data.word;
			this.soundGroups = data.soundGroups;
			this.images = [];
			for (let i in data.images){
				this.images.push(new Image(data.images[i]));
			}
			for (let i in data.matchingImages){
				this.matchingImages.push(new Image(data.matchingImages[i]));
			}
			for (let i in data.notMatchingImages){
				this.notMatchingImages.push(new Image(data.notMatchingImages[i]));
			}
			this.similarWords = data.similarWords;
			this.differentWords = data.differentWords;
			this.wordSoundFile = data.wordSoundFile;
			this.soundGroupsSoundFile = data.soundGroupsSoundFile;
			this.description = data.description;
			this.sentenceGood = data.sentenceGood;
			this.sentenceBad = data.sentenceBad;
		}
	}

	getId(){
		return this._id;
	}
}
