import { HttpHelper } from "../extensions/http-helper";

export class Image {
	private _id:string = "";
	changeDate:Date = new Date();

	constructor(data:any = null){
		if (data){
			this._id = data._id;
			this.changeDate = new Date(data.changeDate);
		}
	}

	getId():string{
		return this._id;
	}

	getImageSource():string{
		if (this.getId() == ""){
			return "/";
		}
		return HttpHelper.host + "/res/images/" + this.getId() + "?v=" + this.changeDate.toJSON(); 
	}
}
