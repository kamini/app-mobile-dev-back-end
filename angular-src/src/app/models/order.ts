import autoBind from 'auto-bind';
import { Word } from './word';
import { WordService } from '../services/word.service';

export class Order {
	private _id:string;
	getId(){
		return this._id;
	}
	wordSet:string;

	constructor(data:any = null){
		if (data){
			this.wordSet = data.wordSet;
			this.wordOrder = data.wordOrder;
		}
	}

	wordOrder:Array<string> = [];
	wordOrderFilled:Array<Word> = [];

	async fillWords(wordService:WordService){
		this.wordOrderFilled = await wordService.getOrderedWords(this.wordSet);
		console.log(this.wordOrderFilled);
	}

	copyFilledWordsToIds(){
		this.wordOrder = [];
		for (let i in this.wordOrderFilled){
			this.wordOrder.push(this.wordOrderFilled[i].getId());
		}
	}
}
