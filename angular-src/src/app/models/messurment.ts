import { Word } from "./word";
import { WordService } from "../services/word.service";

class SingleMessurment{
	wordId:string = "";
	word:Word;
	selectedAnswer:number;

	constructor(data:any = null){
		if (data){
			this.wordId = data.word;
			this.word = new Word(data.word);
			if (this.word){
				this.wordId = this.word.getId();
			}
			this.selectedAnswer = data.selectedAnswer;
		}
	}

	async populateWord(wordService:WordService){
		this.word = await wordService.getWord(this.wordId);
	}
}

export class Messurment {
	private _id: string;
	messurments: Array<SingleMessurment> = [];
	isPreMessurment: boolean;
	group:string;
	date:Date;

	constructor(data:any = null){
		if (data){
			this._id = data._id;
			this.group = data.group;
			this.date = new Date(data.date);
			for (let i in data.messurments){
				this.messurments.push(new SingleMessurment(data.messurments[i]));
			}
			this.isPreMessurment = data.isPreMessurment;
		}
	}

	getId():string {
		return this._id;
	}

	getScore():number{
		let score = 0;
		for (let i in this.messurments){
			score += this.messurments[i].selectedAnswer == 0 ? 1 : 0;
		}
		return score;
	}
}
