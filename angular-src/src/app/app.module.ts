import { MaterializeModule } from 'angular2-materialize';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './app.routing';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { WordsComponent } from './components/words/words.component';
import { RootComponent } from './components/root/root.component';
import { ResultsComponent } from './components/results/results.component';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { WordService } from './services/word.service';
import { EditWordComponent } from './components/edit-word/edit-word.component';
import { MessurmentService } from './services/messurment.service';
import { GroupsComponent } from './components/groups/groups.component';
import { OrderService } from './services/order.service';
import { ImageSelectComponent } from './components/image-select/image-select.component';
import { ImageService } from './services/image.service';
import { DownloadExcelComponent } from './components/download-excel/download-excel.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    WordsComponent,
    RootComponent,
    ResultsComponent,
    EditWordComponent,
    GroupsComponent,
    ImageSelectComponent,
    DownloadExcelComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    MaterializeModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    AuthService,
    WordService,
    MessurmentService,
    OrderService,
    ImageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
