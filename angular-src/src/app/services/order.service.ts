import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHelper } from '../extensions/http-helper';
import autoBind from 'auto-bind';
import { Order } from '../models/order';

@Injectable()
export class OrderService {

	constructor(private http:HttpClient) {
		autoBind(this);
	}

	async getGroups():Promise<Array<string>>{
		let result:any = await this.http.get(HttpHelper.host + "/order/groups").toPromise();
		return result;
	}

	async getOrders():Promise<Array<Order>>{
		let result:any = await this.http.get(HttpHelper.host + "/order").toPromise();
		let processedResult:Array<Order> = [];
		for (let i in result){
			processedResult.push(new Order(result[i]));
		}
		return processedResult;
	}

	async saveOrder(order:Order):Promise<Order>{
		console.log(order);
		let result:any = await this.http.put(HttpHelper.host + "/order/" + order.wordSet, order, {headers:HttpHelper.getAuthorizaedHeaders()}).toPromise();
		console.log(result);
		return new Order(result);
	}
}
