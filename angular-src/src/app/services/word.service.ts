import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHelper } from '../extensions/http-helper';
import autoBind from 'auto-bind';
import { Word } from '../models/word';

@Injectable()
export class WordService {

	constructor(private http:HttpClient) { }

	async getWords():Promise<Array<Word>>{
		let results = await this.http.get(HttpHelper.host + "/word", {headers:HttpHelper.getUnauthorizedHeaders()}).toPromise();
		let words:Array<Word> = [];
		for (let i in results){
			let word = new Word(results[i]);
			words.push(word);
		}
		return words;
	}

	async getOrderedWords(group:string):Promise<Array<Word>>{
		let results = await this.http.get(HttpHelper.host + "/word/ordered/" + group, {headers:HttpHelper.getUnauthorizedHeaders()}).toPromise();
		let words:Array<Word> = [];
		for (let i in results){
			let word = new Word(results[i]);
			words.push(word);
		}
		return words;
	}

	async getWord(wordId:string):Promise<Word>{
		let result = await this.http.get(HttpHelper.host + "/word/" + wordId, {headers:HttpHelper.getUnauthorizedHeaders()}).toPromise();
		return new Word(result);
	}

	async newWord(word:Word):Promise<any>{
		let result = await this.http.post(HttpHelper.host + "/word", word, {headers:HttpHelper.getAuthorizaedHeaders()}).toPromise();
	}

	async editWord(word:Word):Promise<any>{
		let result = await this.http.put(HttpHelper.host + "/word/" + word.getId(), word, {headers:HttpHelper.getAuthorizaedHeaders()}).toPromise();
	}

	async deleteWord(word:Word):Promise<any>{
		let result = await this.http.delete(HttpHelper.host + "/word/" + word.getId(), {headers:HttpHelper.getAuthorizaedHeaders()}).toPromise();
	}
}
