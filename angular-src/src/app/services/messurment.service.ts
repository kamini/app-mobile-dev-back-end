import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import autoBind from 'auto-bind';
import { HttpHelper } from '../extensions/http-helper';
import { Messurment } from '../models/messurment';

@Injectable()
export class MessurmentService {

	constructor(private http: HttpClient) { }

	async getMessurments():Promise<Array<Messurment>>{
		let result = await this.http.get(HttpHelper.host + "/messurment", {headers:HttpHelper.getAuthorizaedHeaders()}).toPromise();
		let messurments = [];
		for (let i in result){
			messurments.push(new Messurment(result[i]));
		}
		return messurments;
	}

	async getMessurment(messurmentId: string):Promise<Messurment>{
		let result = await this.http.get(HttpHelper.host + "/messurment/"+ messurmentId, {headers:HttpHelper.getAuthorizaedHeaders()}).toPromise();
		return new Messurment(result);
	}

	async deleteMessurments():Promise<any>{
		let result = await this.http.delete(HttpHelper.host + "/messurment", {headers:HttpHelper.getAuthorizaedHeaders()}).toPromise();
	}

	async downloadExcel():Promise<any>{
		try{
			let result = await this.http.get(HttpHelper.host + "/messurment/download/xlsx", {headers:HttpHelper.getAuthorizaedHeaders(), responseType:"blob"}).toPromise();
			const url= window.URL.createObjectURL(result);
			let a = document.createElement("a");
			document.body.appendChild(a);
			a.href = url;
			a.download = "result.xlsx";
			a.click();
			a.remove();
		}catch(err){
			console.log(err);
		}
	}
}
