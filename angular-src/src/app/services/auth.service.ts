import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import autoBind from 'auto-bind';
import { HttpHelper } from '../extensions/http-helper';

@Injectable()
export class AuthService {

	loggedInChanged:EventEmitter<boolean> = new EventEmitter();

	constructor(private http:HttpClient) { 
		autoBind(this);
	}

	isLoggedIn():boolean{
		if (!sessionStorage.getItem('auth-token'))
			return false;
		return !!sessionStorage.getItem('auth-token').trim();
	}

	async login(name:string, pass:string):Promise<any>{
		let result:any = await this.http.post(HttpHelper.host + "/auth/login", {name,pass}, {headers: HttpHelper.getUnauthorizedHeaders()}).toPromise();
		sessionStorage.setItem('auth-token', result.token);
		this.loggedInChanged.emit(true);
	}

	logout(){
		sessionStorage.clear();
		this.loggedInChanged.emit(false);
	}
}
