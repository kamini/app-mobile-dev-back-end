import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import autoBind from 'auto-bind';
import { HttpHelper } from '../extensions/http-helper';
import { Image } from '../models/image';

@Injectable()
export class ImageService {

	images: Array<Image> = [];

	constructor(private http:HttpClient) {
		autoBind(this);
		this.fetchImages();
	}

	async fetchImages():Promise<Array<Image>>{
		let result = await this.http.get(HttpHelper.host + "/image", {headers:HttpHelper.getUnauthorizedHeaders()}).toPromise();
		let images:Array<Image> = [];
		for (let i in result){
			images.push(new Image(result[i]));
		}
		this.images = images;
		return images;
	}

	async uploadImage(image):Promise<Image>{
		let formData:FormData = new FormData();
		formData.append("image", image, image.name);
		let result = await this.http.post(HttpHelper.host +"/image", formData, {headers:HttpHelper.getBuilder().addToken().setContentType(null).build()}).toPromise();
		let dbImage = new Image(result);
		await this.fetchImages();
		return dbImage;
	}

	async deleteImage(image:Image):Promise<any>{
		let result = await this.http.delete(HttpHelper.host + "/image/" + image.getId(), {headers:HttpHelper.getAuthorizaedHeaders()}).toPromise();
	}
}
