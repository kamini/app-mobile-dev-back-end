import { Component, OnInit, EventEmitter } from '@angular/core';
import { MessurmentService } from '../../services/messurment.service';
import { Messurment } from '../../models/messurment';
import autoBind from 'auto-bind';
import { toast, MaterializeAction } from 'angular2-materialize';
import { Word } from '../../models/word';

class ResultPerWord{
	word:Word;
	answeredCorrect:number = 0;
	answeredWrong1:number = 0;
	answeredWrong2:number = 0;
	answeredWrong3:number = 0;
	isPreScore:boolean = false;

	getScoreOfImage(i:number):number{
		if (i == 0)
			return this.answeredCorrect;
		if (i == 1)
			return this.answeredWrong1;
		if (i == 2)
			return this.answeredWrong2;
		return this.answeredWrong3;
	}
}

class MessuredGroup{
	messurments:Array<Messurment> = [];
	preScoresPerWord:Array<ResultPerWord> = [];
	postScoresPerWord:Array<ResultPerWord> = [];

	getGroup():String{
		return this.messurments[0].group;
	}

	getAvgPretestScore():number{
		let result = 0;
		let filteredList = this.messurments.filter(function(messurment){
			return messurment.isPreMessurment;
		});
		filteredList.forEach(function(messurment){
			result += ((messurment.getScore() / messurment.messurments.length) * 10);
		});
		return result / filteredList.length;
	}

	getAvgPostTestScore():number{
		let result = 0;
		let filteredList = this.messurments.filter(function(messurment){
			return !messurment.isPreMessurment;
		});
		filteredList.forEach(function(messurment){
			result += ((messurment.getScore() / messurment.messurments.length) * 10);
		});
		return result / filteredList.length;
	}


	getPreScoresPerWord():Array<ResultPerWord>{
		let result:Array<ResultPerWord> = [];
		let filteredList = this.messurments.filter(function(messurment){
			return messurment.isPreMessurment;
		});
		filteredList.forEach(function(messurment){
			for (let i in messurment.messurments){
				let thisWordResult = null;
				for (let c in result){
					if (result[c].word.getId() == messurment.messurments[i].word.getId()){
						thisWordResult = result[c];
					}
				}
				if (!thisWordResult){
					thisWordResult = new ResultPerWord();
					thisWordResult.word = messurment.messurments[i].word;
					result.push(thisWordResult);
				}
				if (messurment.messurments[i].selectedAnswer == 0){
					thisWordResult.answeredCorrect++;
				}else if (messurment.messurments[i].selectedAnswer == 1){
					thisWordResult.answeredWrong1++;
				}else if (messurment.messurments[i].selectedAnswer == 2){
					thisWordResult.answeredWrong2++;
				}else{
					thisWordResult.answeredWrong3++;
				}
				thisWordResult.isPreScore = messurment.isPreMessurment;
			}
		});
		this.preScoresPerWord = result;
		return result;
	}

	getPostScoresPerWord():Array<ResultPerWord>{
		let result:Array<ResultPerWord> = [];
		let filteredList = this.messurments.filter(function(messurment){
			return !messurment.isPreMessurment;
		});
		filteredList.forEach(function(messurment){
			for (let i in messurment.messurments){
				let thisWordResult = null;
				for (let c in result){
					if (result[c].word.getId() == messurment.messurments[i].word.getId()){
						thisWordResult = result[c];
					}
				}
				if (!thisWordResult){
					thisWordResult = new ResultPerWord();
					thisWordResult.word = messurment.messurments[i].word;
					result.push(thisWordResult);
				}
				if (messurment.messurments[i].selectedAnswer == 0){
					thisWordResult.answeredCorrect++;
				}else if (messurment.messurments[i].selectedAnswer == 1){
					thisWordResult.answeredWrong1++;
				}else if (messurment.messurments[i].selectedAnswer == 2){
					thisWordResult.answeredWrong2++;
				}else{
					thisWordResult.answeredWrong3++;
				}
				thisWordResult.isPreScore = messurment.isPreMessurment;
			}
		});
		this.postScoresPerWord = result;
		return result;
	}

	constructor(){
	}
}

@Component({
	selector: 'app-results',
	templateUrl: './results.component.html',
	styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

	messurtGroups:Array<MessuredGroup> = [];
	fetched:boolean = false;
	modalActions = new EventEmitter<string|MaterializeAction>();

	constructor(private messurmentService:MessurmentService) { 
		autoBind(this);
	}

	processMessurments(messurments:Array<Messurment>){
		this.messurtGroups = messurments.reduce(function(groups:Array<MessuredGroup>, item:Messurment){
			for (let i in groups){
				if (groups[i].getGroup() == item.group){
					groups[i].messurments.push(item);
					return groups;
				}
			}
			let messuredGroup = new MessuredGroup();
			messuredGroup.messurments.push(item);
			groups.push(messuredGroup);
			return groups;
		}, []);
		this.messurtGroups.forEach(function(messuredGroup){
			messuredGroup.getPostScoresPerWord();
			messuredGroup.getPreScoresPerWord();
		});
	}

	async ngOnInit() {
		try{
			this.processMessurments(await this.messurmentService.getMessurments());
			this.fetched = true;
		}catch(err){
			toast(err.error.err, 5000);
		}
	}

	confirmDelete(){
		this.modalActions.emit({action:"modal",params:['open']});
	}

	closeModal(){
		this.modalActions.emit({action:"modal",params:['close']});
	}

	async deleteResults(){
		try{
			await this.messurmentService.deleteMessurments();
			toast("Alle resultaten zijn verwijderd.", 5000);
			this.processMessurments(await this.messurmentService.getMessurments());
		}catch(err){
			toast(err.error.err, 5000);
		}
		this.closeModal();
	}
}
