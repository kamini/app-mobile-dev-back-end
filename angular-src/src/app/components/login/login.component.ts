import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import autoBind from 'auto-bind';
import { toast } from 'angular2-materialize';
import { AfterViewChecked } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	username:string = "";
	password:string = "";
	constructor(private authService:AuthService) {
		autoBind(this);
	}

	ngOnInit() {
	}

	async onSubmit(){
		try{
			await this.authService.login(this.username, this.password);
		}catch(err){
			console.log(err);
			toast(err.error.err, 5000);
		}
	}
}
