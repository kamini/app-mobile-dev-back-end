import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import autoBind from 'auto-bind';
import { Word } from '../../models/word';
import { WordService } from '../../services/word.service';
import { toast } from 'angular2-materialize';
import { HttpHelper } from '../../extensions/http-helper';
import { Router } from '@angular/router';
import Materialize from 'materialize-css';
import { AfterViewChecked } from '@angular/core/src/metadata/lifecycle_hooks';
import { Image } from '../../models/image';

@Component({
	selector: 'app-edit-word',
	templateUrl: './edit-word.component.html',
	styleUrls: ['./edit-word.component.css']
})
export class EditWordComponent implements OnInit, OnDestroy, AfterViewChecked {

	title:string = "";
	word:Word = new Word();
	private sub:any;
	wordSoundFile: any = null;
	soundGroupsSoundFile: any = null;
	
	constructor(private route:ActivatedRoute, private wordService:WordService, private router:Router) { 
		autoBind(this);
	}
	
	ngOnInit() {
		this.sub = this.route.params.subscribe(async function(params){
			if (params['wordId'] == null){
				this.word = new Word();
				this.title = "Nieuw woord";
			}else{
				try{
					this.word = await this.wordService.getWord(params['wordId']);
					this.title = "Woord bewerken";
				}catch(err){
					if (err.error && err.error.err)
					toast(err.error.err, 5000);
				else
					toast(err.error, 5000);
				}
			}
			console.log("updating text fields");
		}.bind(this));
	}

	ngAfterViewChecked(): void {
		try{
			Materialize.updateTextFields();
		}catch(err){

		}
	}
	
	ngOnDestroy(): void {
		this.sub.unsubscribe();
	}

	onSoundGroupsChanged(event){
		this.word.soundGroups = event.target.value.replace(", ", ",").split(',');
	}

	onSimilarWordsChanged(event){
		this.word.similarWords = event.target.value.replace(", ", ",").split(',');
	}

	onDifferentWordsChanged(event){
		this.word.differentWords = event.target.value.replace(", ", ",").split(',');
	}

	wordSoundFileChanged(event){
		let files = event.target.files;
		this.wordSoundFile = files ? files[0]: null;
	}

	soundGroupsFileChanged(event){
		let files = event.target.files;
		this.soundGroupsSoundFile = files ? files[0]: null;
	}

	navigateToWords(){
		this.router.navigate(["home", "words"]);
	}

	deleteMatchingImage(image){
		let index = this.word.matchingImages.indexOf(image);
		this.word.matchingImages.splice(index, 1);
	}

	addMatchingImage(){
		this.word.matchingImages.push(new Image());
	}

	deleteNotMatchingImage(image){
		let index = this.word.notMatchingImages.indexOf(image);
		this.word.notMatchingImages.splice(index, 1);
	}

	addNotMatchingImage(){
		console.log("not matching image clicked");
		this.word.notMatchingImages.push(new Image());
		console.log(this.word.notMatchingImages);
		console.log("added");
	}

	async onSubmit(){
		try{
			console.log(this.word);
			for(let i in this.word.soundGroups){
				console.log("sound group " + i);
			}
			if (!this.word.getId()){
				let result = await this.wordService.newWord(this.word);
				console.log(result);
			}
			else{
				await this.wordService.editWord(this.word);
			}
			this.router.navigate(["home", "words"]);
		}catch(err){
			console.log(err);
			if (err.error && err.error.err)
				toast(err.error.err, 5000);
			else
				toast("Er is een probleem met het versturen", 5000);
		}
	}

	getHost():string{
		return HttpHelper.host;
	}

}
