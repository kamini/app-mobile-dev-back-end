import { Component, OnInit, EventEmitter } from '@angular/core';
import { WordService } from '../../services/word.service';
import autoBind from 'auto-bind';
import { Word } from '../../models/word';
import { toast, MaterializeAction } from 'angular2-materialize';

@Component({
	selector: 'app-words',
	templateUrl: './words.component.html',
	styleUrls: ['./words.component.css']
})
export class WordsComponent implements OnInit {

	words:Array<Word> = [];
	modalActions = new EventEmitter<string|MaterializeAction>();
	word:Word = new Word();

	constructor(private wordService:WordService) {
		autoBind(this);
	}

	async ngOnInit() {
		try{
			this.words = await this.wordService.getWords();
		}catch(err){
			console.log(err);
			toast(err.error.err, 5000);
		}
	}

	confirmDeleteWord(word:Word){
		this.word = word;
		this.openModal();
	}

	async deleteWord(){
		try{
			await this.wordService.deleteWord(this.word);
			this.word = new Word();
			this.words = await this.wordService.getWords();
			this.closeModal();
		}catch(err){
			this.words = await this.wordService.getWords();
			this.closeModal();
			console.log(err);
			toast(err.error.err, 5000);
		}
	}

	openModal(){
		this.modalActions.emit({action:"modal",params:['open']});
	}

	closeModal(){
		this.modalActions.emit({action:"modal",params:['close']});
	}

}
