import { Component, OnInit } from '@angular/core';
import { WordService } from '../../services/word.service';
import { OrderService } from '../../services/order.service';
import { Order } from '../../models/order';
import { toast } from 'angular2-materialize';
import { Word } from '../../models/word';
import { Form } from '@angular/forms';
import { NgForm } from '@angular/forms';
import shuffle from 'shuffle-array';

@Component({
	selector: 'app-groups',
	templateUrl: './groups.component.html',
	styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
	groupOrders:Array<Order> = [];
	words:Array<Word> = [];

	constructor(private wordService:WordService, private orderService:OrderService) { }

	async ngOnInit() {
		console.log("on init");
		await this.refresh();
	}

	async refresh(){
		try{
			this.words = await this.wordService.getWords();
			this.groupOrders = await this.orderService.getOrders();
			for (let i in this.groupOrders){
				await this.groupOrders[i].fillWords(this.wordService);
			}
			console.log(this.groupOrders);
		}catch(err){
			console.log(err);
			if (err.error && err.error.err)
				toast(err.error.err, 5000);
			else
				toast("Er is een probleem met het ophalen", 5000);
		}
	}

	async addWord(order:Order, word:Word){
		if(!word){
			toast("Selecteer eerst een woord", 5000);
		} else if (order.wordOrder.indexOf(word.getId()) != -1){
			toast("Dit woord zit al in de lijst", 5000);
		}else{
			try{

				order.wordOrder.push(word.getId());
				console.log(order);
				await this.orderService.saveOrder(order);
			}catch(err){
				if (err.error && err.error.err)
				toast(err.error.err, 5000);
				else
				toast("Er is een error gebeurd", 5000);	
			}
			this.refresh();
		}
	}

	async addAllWords(order:Order){
		let notAddedWords = this.words.filter(function(word){
			if (order.wordOrder.indexOf(word.getId()) == -1){
				return true;
			}
			return false;
		});
		for (let i in notAddedWords){
			order.wordOrder.push(notAddedWords[i].getId());
		}
		try{
			await this.orderService.saveOrder(order);
		}catch(err){
			if (err.error && err.error.err)
				toast(err.error.err, 5000);
			else
				toast("Er is een error gebeurd", 5000);
		}
		this.refresh();
	}

	async shuffleWords(order:Order){
		try{
			shuffle(order.wordOrder);
			await this.orderService.saveOrder(order);
			await this.refresh();
		}catch(err){
			if (err.error && err.error.err)
				toast(err.error.err, 5000);
			else
				toast("Er is een error gebeurd", 5000);
		}
	}

	async removeWord(order:Order, word:Word){
		try{
			order.wordOrder.splice(order.wordOrder.indexOf(word.getId()), 1);
			let index = this.groupOrders.indexOf(order);
			this.groupOrders[index] = await this.orderService.saveOrder(order);
			await this.groupOrders[index].fillWords(this.wordService);
		}catch(err){
			if (err.error && err.error.err)
				toast(err.error.err, 5000);
			else
				toast("Er is een error gebeurd", 5000);
		}
	}

	async increaseIndex(order:Order,word:Word){
		let currentIndex = order.wordOrder.indexOf(word.getId());
		if (!(currentIndex >= order.wordOrder.length -1)){
			let higherItem = order.wordOrder[currentIndex+1];
			order.wordOrder[currentIndex+1] = word.getId();
			order.wordOrder[currentIndex] = higherItem;
			try{
				await this.orderService.saveOrder(order);
				await this.refresh();
			}catch(err){
				if (err.error && err.error.err)
					toast(err.error.err, 5000);
				else
					toast("Er is een error gebeurd", 5000);
			}
		}
	}

	async decreaseIndex(order:Order,word:Word){
		let currentIndex = order.wordOrder.indexOf(word.getId());
		if (currentIndex > 0){
			let lowerItem = order.wordOrder[currentIndex-1];
			order.wordOrder[currentIndex-1] = word.getId();
			order.wordOrder[currentIndex] = lowerItem;
			try{
				await this.orderService.saveOrder(order);
				await this.refresh();
			}catch(err){
				if (err.error && err.error.err)
					toast(err.error.err, 5000);
				else
					toast("Er is een error gebeurd", 5000);
			}
		}
	}
}
