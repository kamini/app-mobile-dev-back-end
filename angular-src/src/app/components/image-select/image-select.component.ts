import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ImageService } from '../../services/image.service';
import { Image } from '../../models/image';
import { toast } from 'angular2-materialize';

@Component({
	selector: 'app-image-select',
	templateUrl: './image-select.component.html',
	styleUrls: ['./image-select.component.css']
})
export class ImageSelectComponent implements OnInit {

	images:Array<Image> = [];
	@Input('image')
	selectedImage:Image = new Image();
	file = null;
	fileField = null;
	isUpload:boolean = false;

	constructor(private imageService:ImageService) {
		this.images = imageService.images;
	}

	setUpload(upload:boolean){
		this.isUpload = upload;
	}

	ngOnInit() {
	}

	fileChanged(event){
		let files = event.target.files;
		this.fileField = event.target;
		this.file = files ? files[0]: null;
	}

	setSelectedImage(image:Image){
		Object.assign(this.selectedImage, image);
	}

	async upload(){
		if (this.file == null){
			toast("Gelieve eerst een afbeelding te kiezen", 5000);
			return;
		}
		try{
			this.setSelectedImage(await this.imageService.uploadImage(this.file));
			this.file = null;
			console.log(this.selectedImage);
			this.fileField.value = '';
			this.isUpload = false;
		}catch(err){
			console.log(err);
			if (err.error && err.error.err)
				toast(err.error.err, 5000);
			else
				toast("Er is een probleem met het versturen", 5000);
		}
	}

	imageClicked(image:Image){
		this.setSelectedImage(image);
	}
}
