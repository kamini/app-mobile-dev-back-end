import { Component, OnInit } from '@angular/core';
import { MessurmentService } from '../../services/messurment.service';

@Component({
  selector: 'app-download-excel',
  templateUrl: './download-excel.component.html',
  styleUrls: ['./download-excel.component.css']
})
export class DownloadExcelComponent implements OnInit {

  constructor(private messurmentService:MessurmentService) { }

  ngOnInit() {
  }

  download(){
	this.messurmentService.downloadExcel();
  }

}
