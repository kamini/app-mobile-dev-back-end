import {Routes} from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { WordsComponent } from './components/words/words.component';
import { RootComponent } from './components/root/root.component';
import { ResultsComponent } from './components/results/results.component';
import { EditWordComponent } from './components/edit-word/edit-word.component';
import { GroupsComponent } from './components/groups/groups.component';

export const routes:Routes = [
	{ path: '', redirectTo: 'home', pathMatch: 'full'},
	{ path: 'login', component: LoginComponent},
	{ path: 'home', component: RootComponent, children: [
		{path: '', redirectTo: 'words', pathMatch: 'full'},
		{path: 'words', component: WordsComponent},
		{path: 'wordsets', component: GroupsComponent},
		{path:'words/edit', component: EditWordComponent},
		{path:'words/edit/:wordId', component: EditWordComponent},
		{path: 'results', component: ResultsComponent}
	]}
];