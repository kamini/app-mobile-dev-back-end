import { HttpHeaders } from "@angular/common/http";
import autoBind from 'auto-bind';


class HeaderBuilder{
	headers:HttpHeaders = new HttpHeaders();

	constructor(){
		autoBind(this);
		this.headers = this.headers.append("Content-Type", "application/json");
	}

	addToken():HeaderBuilder{
		if (!sessionStorage.getItem('auth-token'))
			throw new Error("No authentication token found");
		this.headers = this.headers.append("Authorization", sessionStorage.getItem('auth-token'));
		return this;
	}

	setContentType(type:string):HeaderBuilder{
		if (!type)
			this.headers = this.headers.delete("Content-Type");
		else
			this.headers = this.headers.set('Content-Type', type);
		return this;
	}

	build():HttpHeaders{
		return this.headers;
	}

}

export class HttpHelper {
	static readonly host:string = "http://localhost:3000";

	static getBuilder():HeaderBuilder{
		return new HeaderBuilder();
	}

	static getAuthorizaedHeaders():HttpHeaders{
		return new HeaderBuilder().addToken().build();
	}

	static getUnauthorizedHeaders():HttpHeaders{
		return new HeaderBuilder().build();
	}
}
