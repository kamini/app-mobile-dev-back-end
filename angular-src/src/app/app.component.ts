import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import autoBind from 'auto-bind';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	constructor(private router:Router, private authService:AuthService){
		autoBind(this);

		if (!authService.isLoggedIn())
			this.navigateToLogin();

		authService.loggedInChanged.subscribe(function(isLoggedIn){
			if (isLoggedIn){
				this.navigateToHome();
			}else{
				this.navigateToLogin();
			}
		}.bind(this));
	}

	navigateToHome(){
		this.router.navigate(['home']);
	}

	navigateToLogin(){
		this.router.navigate(['login']);
	}
}
