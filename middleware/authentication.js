const authConfig = require("../config/auth");
const JWT = require("jsonwebtoken");

module.exports = function(req,res,next){
	JWT.verify(req.headers.authorization, authConfig.secret, function(err){
		if (err){
			res.status(401).json({err:"Unauthorized"});
		}else{
			next();
		}
	});
}