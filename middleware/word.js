const Word = require("../models/word");

module.exports.findWord = async function(req,res,next){
	let wordId = req.params.wordId || req.body.wordId || req.query.wordId;
	try{
		req.word = await Word.findById(wordId);
		if (!req.word){
			throw new Error("Word not found!");
		}
		next();
	}catch(err){
		res.status(404).json({err: err.message});
	}
}