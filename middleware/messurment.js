const Messurment = require("../models/messurment");

module.exports.findMessurment = async function(req,res,next){
	let messurmentId = req.params.messurmentId || req.body.messurmentId || req.query.messurmentId;

	try{
		req.messurment = await Messurment.findById(messurmentId);
		if (!req.messurment){
			throw new Error("Messurment not found with id " + messurmentId);
		}
		next();
	}catch(err){
		res.status(404).json({err: err.message});
	}
}