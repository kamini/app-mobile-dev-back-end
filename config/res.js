const resDir = `${__dirname}/../res`;
const imagesDir = resDir + "/images";
const soundDir = resDir + "/sound";

module.exports = {
	resDir: resDir,
	imagesDir: imagesDir,
	soundDir: soundDir
}