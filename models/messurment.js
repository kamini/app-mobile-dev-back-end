const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const idValidator = require("mongoose-id-validator");
const groupsConfig = require("../config/groups");

function selectedAnswerInRange(val){
	return val >= 0 && val <= 3;
}

const MessurmentSchema = new Schema({
	messurments: [{
		word:{
			type: Schema.Types.ObjectId,
			ref: "Word",
			required:true
		},
		selectedAnswer:{
			type: Schema.Types.Number,
			required:true,
			validate: [selectedAnswerInRange, "selectedAnswer should be at least 0 and max 3"]
		}
	}],
	isPreMessurment: {
		type: Schema.Types.Boolean,
		required: true
	},
	group: {
		type: Schema.Types.String,
		required:true,
		enum: groupsConfig.groups
	},
	date: {
		type: Schema.Types.Date,
		requried:true
	}
});

MessurmentSchema.pre('save', function(next){
	let self = this;

	if (self.isNew){
		self.date = new Date();
	}
	next();
});
MessurmentSchema.plugin(idValidator);
MessurmentSchema.pre("find", function(next){
	this.populate({path:'messurments.word'});
	next();
});
MessurmentSchema.pre("findOne", function(next){
	this.populate({path:'messurments.word'});
	next();
});

module.exports = Messurment = mongoose.model('Messurment', MessurmentSchema);