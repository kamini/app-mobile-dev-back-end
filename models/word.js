const mongoose = require("mongoose");
const Schema = mongoose.Schema;

function notToLong(val){
	return val.length < 5;
}

function notToShort(val){
	return val.length > 3;
}

const imagesValidator = [
	{validator: notToShort, msg: 'Not enough images'},
	{validator: notToLong, msg: 'Too many images'}
]

function atLeastOne(val){
	return val.length > 0;
}

const WordSchema = new Schema({
	word: {
		type:Schema.Types.String,
		required: true
	},
	soundGroups:{
		type: [{type:Schema.Types.String, required:true}],
		required: true,
		validate: [atLeastOne, 'soundGroups should contain at least 1 value']
	},
	images: {
		type: [{
			type:Schema.Types.String, required: true,
			ref:"Image"
			
		}],
		required: true,
		validate: imagesValidator
	},
	matchingImages:{
		type: [{
			type:Schema.Types.String, required: true,
			ref:"Image"
		}],
		default: []
	},
	notMatchingImages:{
		type: [{
			type:Schema.Types.String, required: true,
			ref:"Image"
		}],
		default: []
	},
    sentenceGood:{
		type: Schema.Types.String
	},
    sentenceBad:{
        type: Schema.Types.String
    },
	description:{
		type: Schema.Types.String
	},
	similarWords:[{
		type: Schema.Types.String
	}],
	differentWords:[{
		type: Schema.Types.String
	}],
	wordSoundFile:{
		type: Schema.Types.String
	},
	soundGroupsSoundFile:{
		type: Schema.Types.String
	}
});

function autoPopulate(next){
	this.populate('images');
	this.populate('matchingImages');
	this.populate('notMatchingImages');
	next();
}

WordSchema.pre('findOne', autoPopulate);
WordSchema.pre('find', autoPopulate);

module.exports = User = mongoose.model('Word', WordSchema);