const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ImageSchema = new Schema({
	_id:{
		type:Schema.Types.String,
		required:true
	},
	changeDate:{
		type:Schema.Types.Date,
		required: true
	}
});

ImageSchema.pre('validate', function(next){
	this.changeDate = new Date();
	next();
});


module.exports = Image = mongoose.model("Image", ImageSchema);