const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const groupConfig = require("../config/groups");

const OrderSchema = new Schema({
	wordSet:{
		required:true,
		type: Schema.Types.String,
		enum: groupConfig.wordSets,
		unique: true
	},
	wordOrder:[{
		required:true,
		type: Schema.Types.ObjectId,
		ref: "Word"
	}]
});

module.exports = Order = mongoose.model("Order", OrderSchema);