const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const http = require("http");
const fileUpload = require("express-fileupload");
const init = require("./init/init");
init();
mongoose.Promise = global.Promise;

mongoose.connect("mongodb://localhost:27017/mobiledev", {useMongoClient:true}, function(){
	console.log("Connected to mongodb");
});

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(fileUpload());

app.use("/auth", require("./routes/auth"))
app.use("/word", require("./routes/word"));
app.use("/messurment", require("./routes/messurment"));
app.use("/order", require("./routes/order"));
app.use("/image", require("./routes/image"));

app.get("/res/images/:imageName", function(req,res){
	res.sendFile(__dirname + "/res/images/" + req.params.imageName);
});
app.get("/res/sound/:fileName", function(req,res){
	res.sendFile(__dirname + "/res/sound/" + req.params.fileName);
});

app.use("/res", express.static("res"));
app.use("/", express.static("public"));
app.get("/*", function(req,res){
	res.sendFile("./public/index.html", {root: __dirname});
})

const httpServer = http.createServer(app);
httpServer.listen(3000, function(){
	console.log("Server started");
});