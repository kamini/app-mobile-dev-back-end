const express = require("express");
const JWT = require("jsonwebtoken");
const authConfig = require("../config/auth");

const router = express.Router();

router.post("/login", function(req,res){
	let user = null;
	for (let i in authConfig.users){
		if (authConfig.users[i].username == req.body.name && authConfig.users[i].password == req.body.pass){
			user = authConfig.users[i];
		}
	}
	if (user){
		JWT.sign(user, authConfig.secret, {expiresIn: 14400}, function(err, token){
			if (err){
				return res.status(500).json({err});
			}
			res.json({token});
		});
	}else{
		res.status(401).json({err:new Error("Invalid credentials").message});
	}
});

module.exports = router;