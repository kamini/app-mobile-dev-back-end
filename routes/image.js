const Image = require("../models/image");
const express = require("express");
const mAuth = require("../middleware/authentication");
const uuid = require("uuid/v4");
const resConfig = require("../config/res");
const fs = require('fs');

const router = express.Router();

router.get("/",async function(req,res){
	res.json(await Image.find({}));
});

router.get("/:imageId", async function(req,res){
	try{
		res.json(await Image.findById(req.params.imageId));
	}catch(err){
		res.status(500).json({err});
	}
});

router.post("/", mAuth, async function(req,res){
	try{
		if (!req.files || (req.files && !req.files.image)){
			throw new Error("No image provided");
		}
		let file = req.files.image;
		if (file.mimetype != "image/jpeg" && file.mimetype != "image/png"){
			throw new Error("File type of " + file.mimetype + " is not accepted");
		}
		let fileName = uuid();
		let image = new Image();
		image._id = fileName;
		await image.save();
		try{
			await file.mv(resConfig.imagesDir + "/" + fileName);
		}catch(err){
			await image.remove();
			throw err;
		}
		res.json(image);
	}catch(err){
		console.log(err);
		res.status(500).json(err);
	}
});

router.delete("/:imageId", mAuth, async function(req,res){
	try{
		let image = await Image.findById(req.parans.imageId);
		await image.remove();
		res.json({});
	}catch(err){
		res.status(500).json({err});
	}
});

module.exports = router;