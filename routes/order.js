const express = require("express");
const Order = require("../models/order");
const Word = require("../models/word");
const mAuth = require("../middleware/authentication");
const router = express.Router();
const groups = require("../config/groups.js");

router.get("/", async function(req,res){
	try{
		res.json(await Order.find({}));
	}catch(err){
		res.status(500).json({err});
	}
});

router.get("/groups", function(req,res){
	res.json(groups)
});

router.put("/:wordSet", mAuth, async function(req,res){
	try{
		let order = await Order.findOne({wordSet: req.params.wordSet});
		if (!order){
			order = new Order();
		}
		order.wordOrder = req.body.wordOrder;
		await order.save();
		res.json(order);
	}catch(err){
		res.status(500).json({err});
	}
});

module.exports = router;