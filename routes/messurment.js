const express = require("express");
const auth = require("../middleware/authentication");
const Messurment = require("../models/messurment");
const mMessurment = require("../middleware/messurment");
const Word = require("../models/word");
const xl = require("excel4node");
const fs = require("fs");

const router = express.Router();

router.get("/", auth, async function(req,res){
	try{
		res.json(await Messurment.find());
	}catch(err){
		res.status(500).json({err: err.message});
	}
});

router.get("/download/xlsx", auth, async function(req,res){
	let results = await Messurment.find();
	let words = await Word.find();
	let wb = new xl.Workbook();
	let ws = wb.addWorksheet('Sheet 1');

	const headerStyle = wb.createStyle({
		font: {
			color: "#0000FF"
		}
	});

	function getY(row, isPreTest){
		if (isPreTest){
			return 3 + row;
		}
		return 3 + row + words.length + 2;
	}

	function getX(column, groupName){
		group =  parseInt(groupName.replace(/[^\d.]/g,''));
		ws.cell(1,4 * (group - 1) +2).string(groupName).style(headerStyle);
		for (let i = 0; i < 4; i++){
			let text = i == 0 ? "juist" : "afleider " + i;
			ws.cell(2,i + 4 * (group - 1) +2).string(text);
		}
		return column + 4 * (group - 1) +2;
	}

	function getIndexOfWord(word){
		let i = words.findIndex((thisWord)=>{return thisWord._id.equals(word._id)});
		return i;
	}

	words.forEach((word, i)=>{
		ws.cell(getY(i, false),1).string(word.word);
		ws.cell(getY(i, true),1).string(word.word);
		for (let g = 1; g <= 2; g++){
			for (let s = 0; s < 4; s++){
				ws.cell(getY(i, false), getX(s, g+"")).number(0);
				ws.cell(getY(i, true), getX(s, g+"")).number(0);
			}
		}
	});

	ws.cell(getY(0, true)-1, 1).string("Voor test").style(headerStyle);
	ws.cell(getY(0, false)-1, 1).string("Na test").style(headerStyle);

	let vals = {};

	results.forEach((messurment, i)=>{
		messurment.messurments.forEach((result,i)=>{
			let index = getIndexOfWord(result.word);
			let y = getY(index, messurment.isPreMessurment);
			let x = getX(result.selectedAnswer, messurment.group);
			let currentVal = vals[x+","+y] == null ? 0 : vals[x+","+y];
			currentVal+=1;
			vals[x+","+y] = currentVal;
			ws.cell(y,x).number(currentVal);
		});
	});

	let fileName = __dirname + "/result.xlsx";

	wb.write(fileName, function(err, stats){
		res.sendFile(fileName, function(err){
			fs.unlink(fileName, function(err){
				if (err){
					console.log(err);
				}
			});
		})
	});
});

router.get("/:messurmentId", auth, mMessurment.findMessurment, function(req,res){
	res.json(req.messurment);
});


router.post("/", async function(req,res){
	try{
		let messurment = new Messurment(req.body);
		await messurment.save();
		res.json(messurment);
	}catch(err){
		res.status(400).json({err: err.message});
	}
});

router.delete("/:messurmentId", auth, mMessurment.findMessurment, async function(req,res){
	await req.messurment.remove();
	res.json();
});

router.delete("/", auth, async function(req,res){
	try{
		await Messurment.remove({});
		res.json({msg: "Everythig is lost now and unrecoverrable"});
	}catch(err){
		res.status(400).json({err: err.message});
	}
})

module.exports = router;