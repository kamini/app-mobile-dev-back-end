const express = require("express");
const Word = require("../models/word");
const Order = require("../models/order");
const authenticate = require("../middleware/authentication");
const uuid = require("uuid/v4");
const resConfig = require("../config/res");
const fs = require('fs');
const mWord = require("../middleware/word");

const router = express.Router();

router.get("/", async function(req,res){
	try{
		let words = await Word.find();
		res.json(words);
	}catch(err){
		res.status(500).json({err});
	}
});

router.get("/ordered/:set", async function(req,res){
	try{
		let order = await Order.findOne({wordSet:req.params.set}).populate("wordOrder");
		if (!order){
			throw new Error("No such group");
		}
		res.json(order.wordOrder);
	}catch(err){
		res.status(400).json({err});
	}
});

router.get("/:wordId", mWord.findWord, function(req,res){
	res.json(req.word);
});

router.post("/", authenticate, async function(req,res){
	try{
		delete req.body._id;
		let word = new Word(req.body);
		await word.save();
		res.json(word);
	}catch(err){
		res.status(500).json({err});
	}
});

router.put("/:wordId", authenticate, mWord.findWord, async function(req,res){
	try{
		delete req.body._id;
		req.word = await Word.findOneAndUpdate({_id: req.word._id}, req.body, null);
		res.json(req.word);
	}catch(err){
		console.log(err);
		res.status(500).json({err});
	}
});

router.patch("/:wordId", authenticate, mWord.findWord, async function(req,res){
	try{
		console.log(req.files);
		if (typeof(req.body.replaceImagesIndexes) != 'array' ){
			req.body.replaceImagesIndexes = [req.body.replaceImagesIndexes];
		}
		if (req.files){
			if (req.files.images){
				if (typeof(req.files.images) != 'array' ){
					req.files.images = [req.files.images];
				}
				for (let i in req.files.images){
					if (req.files.images[i].mimetype != "image/jpeg" && req.files.images[i].mimetype != "image/png"){
						throw new Error("File type of " + file.mimetype + " is not accepted");
					}
					await req.files.images[i].mv(resConfig.imagesDir + '/' + req.word.images[req.body.replaceImagesIndexes[i]]);
				}
			}
			if (req.files.wordSoundFile){
				if (req.files.wordSoundFile.mimetype != "audio/mpeg" && req.files.wordSoundFile.mimetype != "audio/mp3" )
					throw new Error("File type of " + req.files.wordSoundFile.mimetype + " is not accepted");
					let soundFileName = req.word.wordSoundFile || uuid();
				await req.files.wordSoundFile.mv(resConfig.soundDir + "/" + soundFileName);
			}
			if (req.files.soundGroupsSoundFile){
				if (req.files.soundGroupsSoundFile.mimetype != "audio/mpeg" && req.files.wordSoundFile.mimetype != "audio/mp3" )
					throw new Error("File type of " + req.word.soundGroupsSoundFile.mimetype + " is not accepted");
				let soundFileName = req.word.soundGroupsSoundFile || uuid();
				await req.files.soundGroupsSoundFile.mv(resConfig.soundDir + "/" + soundFileName);
			}
		}
		if (req.body.word){
			req.word.word = req.body.word;
		}
		if (req.body.soundGroups){
			req.word.soundGroups = req.body.soundGroups;
		}
		if (req.body.similarWords){
			req.word.similarWords = req.body.similarWords;
		}
		if (req.body.differentWords){
			req.word.differentWords = req.body.differentWords;
		}
		if (req.body.sentenceGood){
			req.word.sentenceGood = req.body.sentenceGood;
		}
		if (req.body.sentenceBad){
			req.word.sentenceBad = req.body.sentenceBad;
		}
		if (req.body.description){
			req.word.description = req.body.description;
		}
		await req.word.save();
		res.json(req.word);
	}catch(err){
		console.log(err);
		res.status(500).json({err: err.message});
	}
});

router.delete("/:wordId", authenticate, mWord.findWord, async function(req,res){
	for(let i in req.word.images){
		if (typeof(req.word.images[i]) != 'function')
			fs.unlink(resConfig.imagesDir + "/" + req.word.images[i], function(err){
			});
	}
	await req.word.remove();
	res.json();
});

module.exports = router;